package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestHelloHandler(t *testing.T) {

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "hola")
	}))
	l, _ := net.Listen("tcp", ":9496")
	ts.Listener = l
	ts.Start()
	defer ts.Close()

	client := ts.Client()

	options := HandlerOptions{
		Client:     client,
		BackendURL: "http://localhost:9496",
	}

	req, err := http.NewRequest("GET", "localhost:8080/", nil)
	if err != nil {
		t.Fatalf("could not create request: %v", err)
	}

	rec := httptest.NewRecorder()
	options.helloHandler(rec, req)

	res := rec.Result()
	defer res.Body.Close()

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatalf("could not read response: %v", err)
	}

	if res.StatusCode != http.StatusOK {
		fmt.Println("Status code wrong")
		t.Errorf("expected status OK; got %v", res.Status)
	}

	d := string(bytes.TrimSpace(b))
	if err != nil {
		fmt.Println("error")
		t.Fatalf("expected an integer; got %s", b)
	}

	if strings.Compare(d, "hola") != 0 {
		t.Fatal("Dio 1")
	}

}

func TestCustomerAccountsHandler(t *testing.T) {

	tt := []struct {
		name    string
		handler http.HandlerFunc
		err     string
		status  int
	}{
		{name: "Error en response", handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			http.Error(w, "Error en response", http.StatusInternalServerError)
		}),
			err: "Error en response", status: http.StatusInternalServerError},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			ts := httptest.NewUnstartedServer(tc.handler)
			l, _ := net.Listen("tcp", ":9496")
			ts.Listener = l
			ts.Start()
			defer ts.Close()

			client := ts.Client()

			options := HandlerOptions{
				Client:     client,
				BackendURL: "http://localhost:9496/customer/account",
			}

			req, err := http.NewRequest("GET", "localhost:8080/", nil)
			if err != nil {
				t.Fatalf("could not create request: %v\n", err)
			}

			rec := httptest.NewRecorder()
			options.customerAccountsHandler(rec, req)

			res := rec.Result()
			defer res.Body.Close()

			b, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Fatalf("could not read response: %v\n", err)
			}

			if res.StatusCode != tc.status {
				fmt.Println("Status code wrong")
				t.Errorf("expected status %v; got %v\n", tc.status, res.Status)
			}

			d := string(bytes.TrimSpace(b))

			if strings.Compare(strings.TrimSpace(d), strings.TrimSpace(tc.err)) != 0 {
				t.Fatalf("Failed -> expected: %s; got: %s\n", tc.err, d)
			}
		})
	}
}
