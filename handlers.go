package main

import (
	"bytes"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
)

type HandlerOptions struct {
	Client     *http.Client
	BackendURL string
}

// Handle hello calls returning uuid
func (o HandlerOptions) helloHandler(w http.ResponseWriter, r *http.Request) {

	url := o.BackendURL + "/hello"
	log.Debug().Msgf("URL to call: %s", url)

	response, errResp := o.Client.Get(url)
	if errResp != nil {
		log.Error().Msgf("Error en response: %s", errResp.Error())
		http.Error(w, "Error en response: %s", http.StatusInternalServerError)
		return
	}

	defer func() {
		err := response.Body.Close()
		if err != nil {
			log.Error().Msgf("Error closing body")
		}
	}()

	respBody, errData := ioutil.ReadAll(response.Body)
	if errData != nil {
		log.Error().Msgf("failed to read response body %s", errData.Error())
		http.Error(w, "failed to read response body %s", http.StatusInternalServerError)
		return
	}

	log.Debug().Msgf("Received: %v", respBody)

	w.WriteHeader(http.StatusOK)
	_, err := w.Write(respBody)
	if err != nil {
		log.Error().Msgf("failed to write response body %s", err.Error())
		http.Error(w, "failed to write response body %s", http.StatusInternalServerError)
		return
	}
}

// Calls to CustomerAccounts
func (o HandlerOptions) customerAccountsHandler(w http.ResponseWriter, r *http.Request) {

	url := o.BackendURL + r.RequestURI
	log.Debug().Msgf("URL to call: %s", url)

	response, errResp := o.Client.Get(url)
	if errResp != nil {
		log.Error().Msgf("Error en response: %s", errResp.Error())
		http.Error(w, "Error en response", http.StatusInternalServerError)
		return
	}

	defer func() {
		err := response.Body.Close()
		if err != nil {
			log.Error().Msgf("Error closing body")
		}
	}()

	respBody, errData := ioutil.ReadAll(response.Body)
	if errData != nil {
		log.Error().Msgf("failed to read response body %s", errData.Error())
		http.Error(w, "failed to read response body %s", http.StatusInternalServerError)
		return
	}

	log.Debug().Msgf("Received: %v", bytes.TrimSpace(respBody))

	if response.StatusCode != 200 {
		http.Error(w, string(bytes.TrimSpace(respBody)), response.StatusCode)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err := w.Write(respBody)
	if err != nil {
		log.Error().Msgf("failed to write response body %s", err.Error())
		http.Error(w, "failed to write response body %s", http.StatusInternalServerError)
		return
	}

}

// Return data of requested account.
// Receives accountId.
func (o HandlerOptions) customerAccountHandler(w http.ResponseWriter, r *http.Request) {

	url := o.BackendURL + r.RequestURI
	log.Debug().Msgf("URL to call: %s", url)

	response, errResp := o.Client.Get(url)
	if errResp != nil {
		log.Error().Msgf("Error en response: %s", errResp.Error())
		http.Error(w, "Error en response: ", http.StatusBadRequest)
		return
	}

	defer func() {
		err := response.Body.Close()
		if err != nil {
			log.Error().Msgf("Error closing body")
		}
	}()

	respBody, errData := ioutil.ReadAll(response.Body)
	if errData != nil {
		log.Error().Msgf("failed to read response body %s", errData.Error())
		http.Error(w, "failed to read response body ", http.StatusInternalServerError)
		return
	}

	log.Debug().Msgf("Received: %v", respBody)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write(respBody)
	if err != nil {
		log.Error().Msgf("failed to write response body %s", err.Error())
		http.Error(w, "failed to write response body %s", http.StatusInternalServerError)
		return
	}

}

// Retrieves all the customer account details
// Receives the following parameters
// 		accountId - a number with the account
// Returns details of such account
func (o HandlerOptions) customerAccountDetailHandler(w http.ResponseWriter, r *http.Request) {

	url := o.BackendURL + r.RequestURI
	log.Debug().Msgf("URL to call: %s", url)

	response, errResp := o.Client.Get(url)
	if errResp != nil {
		log.Error().Msgf("Error en response: %s", errResp.Error())
		http.Error(w, "Error en response: ", http.StatusBadRequest)
		return
	}

	defer func() {
		err := response.Body.Close()
		if err != nil {
			log.Error().Msgf("Error closing body")
		}
	}()

	respBody, errData := ioutil.ReadAll(response.Body)
	if errData != nil {
		log.Error().Msgf("failed to read response body %s", errData.Error())
		http.Error(w, "failed to read response body", http.StatusInternalServerError)
		return
	}

	log.Debug().Msgf("Received: %v", respBody)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write(respBody)
	if err != nil {
		log.Error().Msgf("failed to write response body %s", err.Error())
		http.Error(w, "failed to write response body %s", http.StatusInternalServerError)
		return
	}
}

// Retrieves all the customer account movements
// Received parameters:
//		accountId - number with the account
// 		sort	  - true or false or not present
//		asc		  - true or false or not present
// Returns the list of movements sorted if requested
func (o HandlerOptions) customerAccountMovementsHandler(w http.ResponseWriter, r *http.Request) {

	url := o.BackendURL + r.RequestURI
	log.Debug().Msgf("URL to call: %s", url)

	response, errResp := o.Client.Get(url)
	if errResp != nil {
		log.Error().Msgf("Error en response: %s", errResp.Error())
		http.Error(w, "Error en response", http.StatusBadRequest)
		return
	}

	defer func() {
		err := response.Body.Close()
		if err != nil {
			log.Error().Msgf("Error closing body")
		}
	}()

	respBody, errData := ioutil.ReadAll(response.Body)
	if errData != nil {
		log.Error().Msgf("failed to read response body %s", errData.Error())
		http.Error(w, "failed to read response body", http.StatusInternalServerError)
		return
	}

	log.Debug().Msgf("Received: %v", respBody)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write(respBody)
	if err != nil {
		log.Error().Msgf("failed to write response body %s", err.Error())
		http.Error(w, "failed to write response body %s", http.StatusInternalServerError)
		return
	}
}

// Retrieves all the customer account movements but will display just the top
// requested after sort them if required.
//		accountId 		- number with the account
// 		totalElements	- number of rows to show
//		asc		  		- true or false or not present
// Returns the list of movements sorted if requested
func (o HandlerOptions) customerAccountMovementsTopHandler(w http.ResponseWriter, r *http.Request) {

	url := o.BackendURL + r.RequestURI
	log.Debug().Msgf("URL to call: %s", url)

	response, errResp := o.Client.Get(url)
	if errResp != nil {
		log.Error().Msgf("Error en response: %s", errResp.Error())
		http.Error(w, "Error en response", http.StatusBadRequest)
		return
	}

	defer func() {
		err := response.Body.Close()
		if err != nil {
			log.Error().Msgf("Error closing body")
		}
	}()

	respBody, errData := ioutil.ReadAll(response.Body)
	if errData != nil {
		log.Error().Msgf("failed to read response body %s", errData.Error())
		http.Error(w, "failed to read response body ", http.StatusInternalServerError)
		return
	}

	log.Debug().Msgf("Received: %v", respBody)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write(respBody)
	if err != nil {
		log.Error().Msgf("failed to write response body %s", err.Error())
		http.Error(w, "failed to write response body %s", http.StatusInternalServerError)
		return
	}
}

// Retrieves all the customer account movements but will display just the top
// requested after sort them if required.
//		accountId 		- number with the account
// Returns the list of movements sorted if requested
func (o HandlerOptions) customerAccountMovementsBalanceHandler(w http.ResponseWriter, r *http.Request) {

	url := o.BackendURL + r.RequestURI
	log.Debug().Msgf("URL to call: %s", url)

	response, errResp := o.Client.Get(url)
	if errResp != nil {
		log.Error().Msgf("Error en response: %s", errResp.Error())
		http.Error(w, "Error en response", http.StatusBadRequest)
		return
	}

	defer func() {
		err := response.Body.Close()
		if err != nil {
			log.Error().Msgf("Error closing body")
		}
	}()

	respBody, errData := ioutil.ReadAll(response.Body)
	if errData != nil {
		log.Error().Msgf("failed to read response body %s", errData.Error())
		http.Error(w, "failed to read response body", http.StatusInternalServerError)
		return
	}

	log.Debug().Msgf("Received: %v", respBody)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write(respBody)
	if err != nil {
		log.Error().Msgf("failed to write response body %s", err.Error())
		http.Error(w, "failed to write response body %s", http.StatusInternalServerError)
		return
	}
}
