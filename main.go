package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"net/http"
	"os"
	"os/signal"
	"time"
)

// Display start messages
func start() {

	fmt.Println("Starting server")

	// Start to read conf file
	fmt.Print("\n\n")
	fmt.Println("=============================================")
	fmt.Println("    Configuration checking - frontend v0.1")
	fmt.Println("=============================================")

	// loading configuration
	viper.SetConfigName("conf") // name of config file (without ext)
	viper.AddConfigPath(".")    // default path for conf file

	viper.SetDefault("port", ":9296")                       // default port value
	viper.SetDefault("backendURL", "http://localhost:9596") // default backend service value
	viper.SetDefault("loglevel", "info")                    // default log level value
	viper.SetDefault("MaxIdleConns", 510)                   // default Concurrents
	viper.SetDefault("MaxIdleConnsPerHost", 510)            // default Concurrents per host

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		fmt.Printf("Fatal error config file: %v \n", err)
		panic(err)
	}

	fmt.Println("-- Using port:                 ", viper.GetString("port"))

	zerolog.TimeFieldFormat = time.RFC3339
	zerolog.TimestampFieldName = "@timestamp"
	switch viper.GetString("loglevel") {
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "disabled":
		zerolog.SetGlobalLevel(zerolog.Disabled)
	default:
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	fmt.Println("-- Log lever set to:    		", viper.GetString("loglevel"))
	fmt.Println("-- Backend service URL: 		", viper.GetString("backendURL"))
	fmt.Println("-- MaxIdleConns:              ", viper.GetString("MaxIdleConns"))
	fmt.Println("-- MaxIdleConnsPerHost:       ", viper.GetString("MaxIdleConnsPerHost"))

	fmt.Println("=============================================")

}

func main() {

	start()

	client := SetClient()

	options := HandlerOptions{
		Client:     client,
		BackendURL: "http://localhost:9596",
	}

	// CPU profiling by default
	//defer profile.Start().Stop()
	// Memory profiling
	//defer profile.Start(profile.MemProfile).Stop()

	// ------------------------------------------
	// /customer/accounts					- OK
	// /customer/account					- OK
	// /customer/account/detail				- OK
	// /customer/account/movements			- OK
	// /customer/account/movements/top		- OK
	// /customer/account/movements/balance	- OK
	// ------------------------------------------

	r := mux.NewRouter()
	r.Path("/hello").HandlerFunc(options.helloHandler).Methods("GET")
	r.Path("/customer/accounts").Queries().HandlerFunc(
		options.customerAccountsHandler).Methods("GET")
	r.Path("/customer/account").Queries().HandlerFunc(
		options.customerAccountHandler).Methods("GET")
	r.Path("/customer/account/detail").Queries().HandlerFunc(
		options.customerAccountDetailHandler).Methods("GET")
	r.Path("/customer/account/movements").Queries().HandlerFunc(
		options.customerAccountMovementsHandler).Methods("GET")
	r.Path("/customer/account/movements/top").Queries().HandlerFunc(
		options.customerAccountMovementsTopHandler).Methods("GET")
	r.Path("/customer/account/movements/balance").Queries().HandlerFunc(
		options.customerAccountMovementsBalanceHandler).Methods("GET")

	srv := &http.Server{
		Addr:        viper.GetString("port"),
		Handler:     r,
		ReadTimeout: 10 * time.Second,
		//WriteTimeout:   10 * time.Second,
		//MaxHeaderBytes: 1 << 20,
	}

	// Lanuch server in a thread
	go func() {
		fmt.Println("Starting server...")
		log.Info().Msg("Starting server...")
		if err := srv.ListenAndServe(); err != nil {
			log.Panic().Msgf("%s", err)
		}
	}()

	// Process a graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	errShutdown := srv.Shutdown(ctx)
	if errShutdown != nil {
		panic(fmt.Sprintf("Error shutting down %s", errShutdown))
	}

	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	fmt.Print("\n\n")
	fmt.Println("shutting down")
	fmt.Println("Goddbye!....")
	os.Exit(0)

}
