OUTDIR = ${GOPATH}/bin/frontend
VERSION = 0.1

# Compile for macOS 
normal: clean
	env go build -o $(OUTDIR)/frontend_mac
	cp conf.yaml $(OUTDIR)/.
	
# Generate code for linux
linux: clean
	env GOOS=linux GOARCH=amd64 go build -o $(OUTDIR)/frontend_linux
	cp conf.yaml $(OUTDIR)/.
	zip -9 $(OUTDIR)/frontend_linux_v$(VERSION).zip $(OUTDIR)/frontend_linux $(OUTDIR)/conf.yaml

# Generate code for windows
windows: clean
	env GOOS=windows GOARCH=amd64 go build -o $(OUTDIR)/frontend_amd64.exe
	cp conf.yaml $(OUTDIR)/.
	zip -9 $(OUTDIR)/frontend_win_v$(VERSION).zip $(OUTDIR)/frontend_amd64.exe $(OUTDIR)/conf.yaml

# Clean all old files
clean:
	rm -f $(OUTDIR)/fronte*
	rm -f $(OUTDIR)/*.exe
